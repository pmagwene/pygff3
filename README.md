

# pygff3


A python library for working with GFF3 format files.

NOTE: This library requires Biopython but was developed before the BCBio.GFF appeared in Biopython.  I find gff.py a bit more Pythonic but YMMV.



### Examples of use


From the shell, download a GFF3 file to work with:

```
$ curl -O http://downloads.yeastgenome.org/curation/chromosomal_feature/saccharomyces_cerevisiae.gff
```


#### Parsing the GFF file

```
>>> ftrs = gff.parse_GFFfeatures('saccharomyces_cerevisiae.gff')
>>> len(ftrs)
22972
```

#### Examining the types of features

```
>>> ftrtypes = {i.type for i in ftrs} # set comprehension, Python 2.7+
>>> len(ftrtypes)
36
>>> ftrtypes
set(['nucleotide_match', 'rRNA', 'telomeric_repeat', 'Y_prime_element', 'binding_site', 'insertion', 'noncoding_exon', 'ncRNA', 'centromere_DNA_Element_I', 'chromosome', 'centromere_DNA_Element_II', 'intron', 'external_transcribed_spacer_region', 'gene_cassette', 'five_prime_UTR_intron', 'LTR_retrotransposon', 'transposable_element_gene', 'centromere', 'centromere_DNA_Element_III', 'pseudogene', 'snRNA', 'telomere', 'long_terminal_repeat', 'X_element', 'mRNA', 'ARS', 'repeat_region', 'tRNA', 'region', 'non_transcribed_region', 'internal_transcribed_spacer_region', 'plus_1_translational_frameshift', 'CDS', 'snoRNA', 'gene', 'X_element_combinatorial_repeat'])
```

#### Getting a particular gene

You can get genes by ID or Alias:

```
>>> import gfftools

>>> ACT1 = gfftools.get_gene(ftrs, 'ACT1')
>>> ACT1
(chrVI, 53260, 54696, -)
>>> ACT1.start
53260
>>> ACT1.end
54696
>>> ACT1.strand
'-'
>>> FLO11 = gfftools.get_gene(ftrs, 'YIR019C')
>>> FLO11
(chrIX, 389572, 393675, -)
```

#### Examining the children associated with a feature

```
>>> ACT1.children
[(chrVI, 53260, 54696, -)]
>>> ACT1.children[0].type
'mRNA'
>>> ACT1.children[0].children
[(chrVI, 53260, 54377, -), (chrVI, 54378, 54686, -), (chrVI, 54687, 54696, -)]
>>> [i.type for i in ACT1.children[0].children]
['CDS', 'intron', 'CDS']
```

#### Translating sequences

By default the `parse_GFFfeatures()` function will "stitch" together the coding sequences (CDS) associated with gene features (see `stitchcds` argument).  This meaans that for features of type gene, the `.cds` attribute returns the associated sequences minus any introns, so that the `.translate()` method returns the canonical peptide sequence:

```
>>> ACT1.cds
Seq('TTAGAAACACTTGTGGTGAACGATAGATGGACCACTTTCGTCGTATTCTTGTTT...CAT', SingleLetterAlphabet())
>>> ACT1.translate()
Seq('MDSEVAALVIDNGSGMCKAGFAGDDAPRAVFPSIVGRPRHQGIMVGMGQKDSYV...CF*', HasStopCodon(ExtendedIUPACProtein(), '*'))
```

There is currently no facility in the library for dealing with alternate splicing.
