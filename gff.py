"""gff.py -- a module for parsing and manipulating genomic features, as defined
in a GFF3 formatted file.


"""


import csv, copy, io
import urllib

from Bio import SeqIO, SeqRecord


# ------------------------------------------------------------------------------#


def fasta_asdict(filename):
    """Parses FASTA file to dictionary. key = sequence IDs, vals = Bio.Seq objects.
    """
    f = open(filename, "rU")
    fdict = SeqIO.to_dict(SeqIO.parse(f, "fasta"))
    f.close()
    gdict = {}
    for key in fdict:
        gdict[key] = fdict[key].seq
    return gdict


def fasta_asdict_fromstr(s):
    """Parses FASTA sequences from string (in memory) to dict.
    """
    f = io.StringIO(s)
    fdict = SeqIO.to_dict(SeqIO.parse(f, "fasta"))
    f.close()
    gdict = {}
    for key in fdict:
        gdict[key] = fdict[key].seq
    return gdict


def fasta_asdict_fromGFF(gff_file):
    """Parses FASTA sequences from a GFF file, if available.
    """
    f = open(gff_file, "r")
    s = f.read()
    fparts = s.split("##FASTA")
    if len(fparts) == 2:
        fastastr = fparts[1]
        gdict = fasta_asdict_fromstr(fastastr)
        return gdict
    else:
        raise Exception("No FASTA sequences in GFF file")


# ------------------------------------------------------------------------------#


class GFFfeature:
    """Class for representing GFF features.
    """

    def __init__(self):
        # the nine standard GFF fields
        self.seqid = None
        self.source = None
        self.type = None
        self.start = None
        self.end = None
        self.score = None
        self.strand = None
        self.phase = None
        self.attributes = None

        # the ten standard reserved attributes
        self.ID = None
        self.Name = None
        self.Alias = None
        self.Parent = None
        self.Target = None
        self.Gap = None
        self.Derives_from = None
        self.Note = None
        self.Dbxref = None
        self.Ontology_term = None

        # some attributes I've added for convenience
        self.sequence = None
        self.isgene = False
        self.children = None
        self.cds = None
        self.parts = None
        self.exons = None
        self.introns = None

    def __repr__(self):
        return "(%s, %s, %s, %s)" % (
            str(self.seqid),
            str(self.start),
            str(self.end),
            self.strand,
        )

    def namestr(self):
        return "%s, %s" % self.Name, str(self.Alias)

    def parse_attribute_str(self):
        """ Modifies feature object in place.
        """
        if self.attributes is None:
            return None
        if self.attributes == ".":
            return None
        attribs = self.attributes.split(";")
        for attrib in attribs:
            attsplit = attrib.split("=")
            aname = attsplit[0]
            atts = set(attsplit[1].split(","))
            eatts = set()
            for att in atts:
                eatts.add(urllib.parse.unquote(att))
            if len(eatts) == 0:
                val = None
            elif len(eatts) == 1:
                val = eatts.pop()
            else:
                val = eatts
            self.__dict__[aname] = val

        if self.type == "gene":
            self.isgene = True

    def assign_sequence(self, genomedict):
        if self.seqid is None:
            return None
        if (self.start is None) or (self.end is None):
            return None
        try:
            chromseq = genomedict[self.seqid]
        except KeyError:
            self.sequence = None
            return None
        start = self.start - 1
        end = self.end
        seq = chromseq[start:end]
        self.sequence = seq

    def translate(self, table=None):
        if self.cds is None:
            return None
        if self.strand == "-":
            if table:
                return self.cds.reverse_complement().translate(table=table)
            else:
                return self.cds.reverse_complement().translate()
        else:
            if table:
                return self.cds.translate(table=table)
            else:
                return self.cds.translate()

    def genomicseq(self):
        if self.strand == "-":
            return self.sequence.reverse_complement()
        else:
            return self.sequence


# ------------------------------------------------------------------------------#


def parse_GFFfeatures(gff_file, gdict=None, stitchcds=True):
    """ Parse a GFF3 file. Returns a list of GFFfeature instances representing each line of the file.

    Arguments:
      * gff_file -- filename
      * gdict -- associate a dictionary of FASTA sequences with the parsed features. 
            If gdict is None and the GFF file has genomic sequence data appended 
            to it (see GFF3 specification) than this information will be parsed directly
            from the GFF file.
      * stitchcds -- specifies whether the individual CDS features (usually exons)
           associated with a gene feature should be "stitched" together.

    """
    fieldlist = [
        "seqid",
        "source",
        "type",
        "start",
        "end",
        "score",
        "strand",
        "phase",
        "attributes",
    ]
    ftrlist, metafields, comments = [], [], []

    f = open(gff_file, "r")
    s = f.read()

    # Check whether FASTA sequences are appended to GFF file
    fparts = s.split("##FASTA")
    if (len(fparts) == 2) and (gdict is None):
        gdict = fasta_asdict_fromstr(fparts[1])

    # parse the GFF fields
    rdr = csv.reader(io.StringIO(fparts[0]), delimiter="\t")
    for row in rdr:
        if row[0][0] == "#":
            if row[0][:2] == "##":
                metafields.append(row)
                continue
            else:
                comments.append(row)
                continue
        ftr = GFFfeature()
        for i, field in enumerate(fieldlist):
            val = row[i]
            if row[i] == "":
                val = None
                continue
            if field == "start" or field == "end":
                try:
                    val = int(row[i])
                except ValueError:
                    val = row[i]
            if field == "score":
                try:
                    val = float(row[i])
                except ValueError:
                    val = row[i]
            ftr.__dict__[field] = val
        ftr.parse_attribute_str()
        if gdict is not None:
            ftr.assign_sequence(gdict)
        ftrlist.append(ftr)
    map_parent_child(ftrlist)
    if stitchcds:
        stitch_gene_cds(ftrlist)
    return ftrlist


# ------------------------------------------------------------------------------#


def map_parent_child(flist):
    """ creates mappings of parent IDs -> child features
    
    my reading of the GFF3 v1.14 spec (Aug 2008) is that the Parent field
    gives the ID of the parent feature.
    """
    byid = {}
    for ftr in flist:
        if ftr.ID is not None:
            byid[ftr.ID] = ftr
    # c2p = {}
    p2c = {}
    for ftr in flist:
        if ftr.Parent is None:
            continue
        elif type(ftr.Parent) == type(""):
            p2c.setdefault(ftr.Parent, []).append(ftr)
        elif type(ftr.Parent) == type([]):
            for p in ftr.Parent:
                p2c.setdefault(p, []).append(ftr)
    for p in p2c:
        byid[p].children = p2c[p]
        byid[p].children.sort(key=lambda obj: obj.start)


def expand_children(ftr):
    c = []
    if ftr.children is None:
        return [ftr]
    for child in ftr.children:
        e = expand_children(child)
        c += e
    c.sort(key=lambda obj: obj.start)
    return c


def stitch_seqs(flist):
    if None in [i.sequence for i in flist]:
        return None
    nseq = flist[0].sequence
    for ftr in flist[1:]:
        nseq += ftr.sequence
    return nseq


def combine_child_cds(ftr):
    c = expand_children(ftr)
    cds = [i for i in c if i.type == "CDS"]
    if not len(cds):
        return None
    seq = stitch_seqs(cds)
    return seq


def stitch_gene_cds(flist):
    for ftr in flist:
        if ftr.type == "gene":
            ftr.cds = combine_child_cds(ftr)
        if ftr.type == "CDS":
            ftr.cds = ftr.sequence

