from gff import GFFfeature

#------------------------------------------------------------------------------#   

def test_overlap(ftr1, ftr2):
    if ftr1.seqid != ftr2.seqid:
        return False
    if ftr1.start <= ftr2.start:
        if ftr2.start <= ftr1.end:
            return True
    if ftr2.start <= ftr1.start:
        if ftr1.start <= ftr2.end:
            return True
    return False
    
def span_feature(f1, f2, gdict):
    if f1.seqid != f2.seqid:
        return None # i.e. not on same chromosome
    if f1.start <= f2.start:
        start = f1.start
    else:
        start = f2.start
    if f1.end >= f2.end:
        end = f1.end
    else:
        end = f2.end
    if f1.strand == f2.strand:
        strand = f1.strand
    else:
        strand = '.'
        
    nftr = GFFfeature()
    nftr.seqid = f1.seqid
    nftr.source = 'User'
    nftr.type = "spanned_feature"
    nftr.start = start
    nftr.end = end
    nftr.score = '.'    
    nftr.strand = strand
    nftr.phase = '.'
    nftr.attributes = "Note=spans %s,%s" % (f1.ID, f2.ID)
    nftr.ID = '%s_%s' % (f1.ID,f2.ID)
    nftr.parse_attribute_str()
    nftr.sequence = gdict[f1.seqid][start-1:end]
    return nftr

def combine_overlapping(flist, gdict):
    n = len(flist)
    combined = []
    combo = None
    for i,current in enumerate(flist):
        if i >= n-1:
            break
        next = flist[i+1] 
        if not test_overlap(current,next):
            if combo is None:
                combined.append(current)
            else:
                sftr = span_feature(combo,current,gdict)
                combined.append(sftr)
                combo = None
        else:
            if combo is None:
                combo = current
            else:
                combo = span_feature(combo,current,gdict)
    return combined
        
        
 

def upstream_of(ftr, n, genomedict, asseq=False):
    chrom = ftr.seqid
    if chrom is None:
        return None
    if (ftr.start is None) or (ftr.end is None):
        return None
    chromseq = genomedict[chrom]
    if ftr.strand == '-':
        ustart = ftr.end
        uend = ftr.end + n
    else:
        ustart = ftr.start - n - 1
        uend = ftr.start - 1
    if ustart < 0:
        ustart = 0
    seq = chromseq[ustart:uend]
    if asseq:
        return seq
    nftr = GFFfeature()
    nftr.seqid = chrom
    nftr.source = 'User'
    nftr.type = "upstream_sequence"
    nftr.ID = nftr.Name = 'upstream_of_%s' % ftr.ID
    nftr.start = ustart + 1
    nftr.end = uend
    nftr.score = '.'    
    nftr.strand = ftr.strand
    nftr.phase = '.'
    nftr.attributes = "Note=%d bases upstream of (%s,%d,%d,%s)" % (n, ftr.seqid, ftr.start, ftr.end, ftr.strand)
    nftr.parse_attribute_str()
    nftr.sequence = seq
    return nftr

    
def downstream_of(ftr, n, genomedict, asseq=False):
    chrom = ftr.seqid
    if chrom is None:
        return None
    if (ftr.start is None) or (ftr.end is None):
        return None
    chromseq = genomedict[chrom]
    if ftr.strand == '-':
        dstart = ftr.start - n - 1        
        dend = ftr.start - 1
    else:
        dstart = ftr.end
        dend = ftr.end + n
    if dstart < 0:
        dstart = 0
    seq = chromseq[dstart:dend]
    if asseq:
        return seq
    nftr = GFFfeature()
    nftr.seqid = chrom
    nftr.source = 'User'
    nftr.type = "downstream_sequence"
    nftr.ID = nftr.Name = 'downstream_of_%s' % ftr.ID
    nftr.start = dstart + 1
    nftr.end = dend
    nftr.score = '.'    
    nftr.strand = ftr.strand
    nftr.phase = '.'
    nftr.attributes = "Note=%d bases downstream of (%s,%d,%d,%s)" % (n, ftr.seqid, ftr.start, ftr.end, ftr.strand)
    nftr.parse_attribute_str()
    nftr.sequence = seq
    return nftr
    
def region_asftr(seqftr,srange):
    """ 1 indexed
    """
    chrom = seqftr.seqid
    nftr = GFFfeature()
    nftr.seqid = chrom
    nftr.source = 'User'
    nftr.type = "sequence_ftr"
    nftr.start = seqftr.start + srange[0] - 1
    nftr.end = seqftr.start + srange[1] - 1
    nftr.score = '.'    
    nftr.strand = seqftr.strand
    nftr.phase = '.'
    nftr.attributes = "Note=%d - %d  of (%s,%d,%d,%s)" % (srange[0],srange[1], seqftr.seqid, seqftr.start, seqftr.end, seqftr.strand)
    nftr.parse_attribute_str()
    nftr.sequence = seqftr.sequence[srange[0]-1:srange[1]]
    return nftr
    
    
def flanking_ftrs(ftr,ftrlist):
    byc = {}
    for f in ftrlist:
        byc.setdefault(f.seqid,[]).append(f)
    c = byc[ftr.seqid]
    if ftr not in c:
        c.append(ftr)
    c.sort(key = lambda obj: obj.start)
    fidx = c.index(ftr)
    lidx = max(0,fidx - 1)
    if fidx == 0:
        lftr = None
    else:
        lftr = c[lidx]
    ridx = min(len(c)-1,fidx + 1)
    if fidx == len(c) - 1:
        rftr = None
    else:
        rftr = c[ridx]
    return lftr,rftr

def surrounding_ftr(ftr, ftrlist, dist):
    byc = {}
    for f in ftrlist:
        byc.setdefault(f.seqid,[]).append(f)
    c = byc[ftr.seqid]
    if ftr not in c:
        c.append(ftr)
    c.sort(key = lambda obj: obj.start)
    fidx = c.index(ftr)
    
    left = []
    lct = 1
    while 1:
        lidx = max(0,fidx - lct)
        if lidx == 0:
            break
        lftr = c[lidx]
        if lftr.end > (ftr.start-dist):
            left.append(lftr)
            lct += 1
        else:
            break
    left.reverse()
    
    right = []
    rct = 1
    while 1:
        ridx = min(len(c)-1,fidx + rct)
        if ridx == len(c)-1:
            break
        rftr = c[ridx]
        if rftr.start < (ftr.end+dist):
            right.append(rftr)
            rct += 1
        else:
            break
    if fidx == 0: left = []
    if fidx == len(c) - 1: right = [] 
    return left, right


def features_at_location(flist, chrom, coord):
    inchrom = [ftr for ftr in flist if ftr.seqid == chrom]
    hits = [ftr for ftr in inchrom if ftr.start <= coord <= ftr.end]
    return hits
    
def features_in_range(flist, chrom, srange):
    inchrom = [ftr for ftr in flist if ftr.seqid == chrom]
    start, end = srange
    hits = [ftr for ftr in inchrom if (start <= ftr.start <= end) or (start <= ftr.end <= end)]
    return hits    

    
#------------------------------------------------------------------------------#   

def gene_name_or_id(ftr):
    if hasattr(ftr, 'gene'):
        return ftr.gene
    else:
        return ftr.ID

def gene_name_and_id(ftr):
    if hasattr(ftr, 'gene'):
        return "%s/%s" % (ftr.gene, ftr.ID)
    else:
        return ftr.ID        

def get_gene(flist, gname):
    genes = [i for i in flist if i.isgene]
    # hits = [i for i in genes if hasattr(i,'gene') and \
    #                             (i.gene == gname or i.ID == gname)]
    hits = [i for i in genes if (i.ID == gname) or (hasattr(i,'gene') and i.gene == gname) ]                            
    if not len(hits):
        return None
    return hits[0]
    
    
def get_genelist(flist, gnames):
    genes = [get_gene(flist,i) for i in gnames]
    return genes
        
    
#------------------------------------------------------------------------------#   

def eqsone(obj, *vals):
    for v in vals:
        if obj == v: return True
    return False

#------------------------------------------------------------------------------#   
